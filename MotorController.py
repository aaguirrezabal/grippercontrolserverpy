# Copyright (c) 2014, Andoni Aguirrezabal
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Gripper Defines
from GripperDefines import *

# UART Interface
from UARTInterface import UARTInterface

# GPIO Interface
import RPi.GPIO as GPIO

# Other Python Imports
import time


class MotorController:
    def __init__(self):
        # Initialize UART for Motor Controller Comm
        self.uartInterface = UARTInterface()

        # Motor Status
        self.brakeOn = False
        self.isMoving = False
        self.currentSpeed = 0
        self.motorDirection = GDIR_CLOSE

        # Initialize GPIO for Motor Controller Brake
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(16, GPIO.OUT)
        self.setbrake()

    def __del__(self):
        # Engage Brake
        self.setbrake()

        # Stop GPIO
        GPIO.cleanup()

    # Motor Controller Requires High Signal for Brake Release
    def releasebrake(self):
        GPIO.output(16, True)
        self.brakeOn = False

    def beginmoving(self):
        if not self.isMoving:
            self.releasebrake()
            self.isMoving = True
            self.setspeed(self.currentSpeed)

    def getstatus(self):
        if self.sendandconfirm(MCMD_STATUS):
            time.sleep(0.1)
            return self.uartInterface.getdata()
        else:
            print "[ERROR] MMBE_STATUS Failed...."
            return False

    def reversedirection(self):
        if self.brakeOn:
            if self.sendandconfirm(MCMD_REVERSEDIR):
                # I don't lke using inline ternary operator, sorry ppl - Andoni
                if self.motorDirection == GDIR_CLOSE:
                    self.motorDirection = GDIR_OPEN
                else:
                    self.motorDirection = GDIR_CLOSE
                return True
            else:
                # TODO: Might have changed direction
                return False
        else:
            return False

    def setbrake(self):
        # Call Line Brake
        GPIO.output(16, False)
        self.brakeOn = True

        # Call MMBe STOP
        if self.sendandconfirm(MCMD_STOP):
            self.isMoving = False
            return True
        else:
            print "[ERROR] MMBE_STOP Failed...."
            return False

    def setspeed(self, speed):
        if speed > 255:
            speed = 255
        elif speed < 0:
            speed = 0

        self.currentSpeed = int(speed)
        print("Speed set to: ", self.currentSpeed)

        if self.isMoving:
            if not self.sendandconfirm(MCMD_SETSPEED, [self.currentSpeed]):
                print "Could not change motor speed, stopping..."
                self.setbrake()

    # Send a command to the motor controller, and verify the response
    def sendandconfirm(self, command, args=[]):
        # Send Command
        if self.sendcommand(command, args):
            # Verify Output
            output = ord(self.uartInterface.getdata(1))
            if output == 170:
                return True
            else:
                return False
        else:
            return False

    # Send a command to the motor controller
    def sendcommand(self, command, args=[]):
        outputMsg = [MCMD_SYNC]  # Add MMBe Sync Command
        outputMsg.append(command)  # Add MMBe Command
        outputMsg = outputMsg + args  # Arguments Payload
        return self.uartInterface.senddata(outputMsg)

