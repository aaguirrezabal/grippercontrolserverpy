# Copyright (c) 2014, Andoni Aguirrezabal
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Thread Components
import threading

# Socket Components
import socket

# Server Components
import ADCReader
import ServerConfig


class SensorStreamer:
    def __init__(self):
        # Variable Init
        self.distanceValues = {0, 0}
        self.pressureValues = {0, 0, 0, 0, 0, 0, 0, 0}

        # TCP Socket Init
        self.SensorSocketTarget = ("0.0.0.0", ServerConfig.SENSOR_PORT)
        self.SensorSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.SensorSocket.bind(self.SensorSocketTarget)

        # Clients List
        self.SensorSocketClientList = []

        # ADC Reader Module Init
        self.distanceAdcModule = ADCReader.ADCReader(0, 0, 1)
        self.pressureAdcModule = ADCReader.ADCReader(0, 1, 0)
        self.adcReaderThread = threading.Thread(target=self.adcLoop)
        self.adcReaderThread.start()

    def __del__(self):
        self.SensorSocket.close()

    def adcLoop(self):
        while True:
            # Read the Data
            # self.distanceValue = self.adcModule.readadc(0)
            for client in self.SensorSocketClientList:
                try:
                    client.send(str(self.distanceValue).zfill(3) + ":" + str(0).zfill(4) + "\n")
                except socket.error:
                    client.close()  # Assume User Disconnected
                    self.SensorSocketClientList.remove(client)

    def mainLoop(self):
        self.SensorSocket.listen(1)
        while True:
            clientConnection, clientAddr = self.SensorSocket.accept()
            self.SensorSocketClientList.append(clientConnection)
            print "New Sensor Client Connected: ", clientAddr