#!/usr/bin/env python

# Copyright (c) 2014, Andoni Aguirrezabal
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Debugging
import pydevd

# Gripper Modules
import ServerConfig
from SensorStreamer import SensorStreamer
from MotorController import MotorController

# Thread Components
import threading

# Socket Components
import socket

# Error Codes
# import errno


class GripperControlServer:
    def __init__(self):
        # LOAD CONFIGURATION
        ServerConfig.loadconfig()

        # Create Motor Controller Module
        self.motorController = MotorController()

        # Create Sensor Streamer Module
        self.sensorStreamer = SensorStreamer()
        self.sensorThread = threading.Thread(target=self.sensorStreamer.mainLoop)

        # Create Control Network Module
        self.ControlSocketTarget = ("0.0.0.0", ServerConfig.CONTROL_PORT)
        self.ControlSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.ControlSocket.bind(self.ControlSocketTarget)
        self.ControlSocketClientList = []

        # Create TCP Control Loop Thread
        self.tcpControlThread = threading.Thread(target=self.controlLoop)

    def __del__(self):
        self.ControlSocket.close()

    def controlLoop(self):
        while True:
            for client in self.ControlSocketClientList:
                try:
                    data = client.recv(32)  # TODO: MAKE NOT BLOCKING
                    commands = data.strip().split('\n')
                    for commandMsg in commands:
                        # commandMsg.strip()  # TODO: CHECK NECESSITY
                        if len(commandMsg) > 0:
                            self.handleCommand(commandMsg)
                except socket.error, e:
                    client.close()  # Assume User Disconnected
                    self.ControlSocketClientList.remove(client)

    def handleCommand(self, commandMsg):
        command = commandMsg[0]
        if (len(commandMsg) > 1):
            args = int(commandMsg[1:])

        if command == "!":  # STOP
            self.motorController.setbrake()
        elif command == "$":  # SPEED
            self.motorController.setspeed(args)
        elif command == "#":  # REVERSE DIRECTION
            self.motorController.reversedirection()
        elif command == "@":  # COMMAND POSITION
            self.motorController.beginmoving()
        else:
            pass

    def main(self):
        # Launch ADC Sensor Thread
        print "Starting Sensor Streamer..."
        self.sensorThread.start()

        # TCP Socket Init
        self.ControlSocket.listen(1)
        print "Control Socket opened, waiting for connections..."

        # Start TCP Control Loop
        self.tcpControlThread.start()

        while True:
            clientConnection, clientAddr = self.ControlSocket.accept()
            self.ControlSocketClientList.append(clientConnection)
            print "New Control Client Connected: ", clientAddr

if __name__ == "__main__":
    app = GripperControlServer()
    pydevd.settrace('10.42.0.1', port=2828, stdoutToServer=True, stderrToServer=True)
    app.main()

