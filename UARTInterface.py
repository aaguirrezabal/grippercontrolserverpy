# Copyright (c) 2014, Andoni Aguirrezabal
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Serial Interface
import serial
from serial.serialutil import EIGHTBITS, PARITY_NONE, STOPBITS_ONE


class UARTInterface:
    def __init__(self):
        # Initialize UART for Motor Controller
        self.uartPort = serial.Serial("/dev/ttyAMA0", baudrate=2400, bytesize=EIGHTBITS,
                                      parity=PARITY_NONE, stopbits=STOPBITS_ONE)
        self.uartPort.open()
        self.uartPort.flushInput()  # Discard the input buffer
        print "DEBUG: UART Interface Opened"

    def __del__(self):
        self.uartPort.flushInput()  # Discard the input buffer
        self.uartPort.flushOutput()  # Discard the output buffer (MAYBE)
        self.uartPort.close()
        print "DEBUG: UART Interface Closed"

    def getdata(self, limit=1):
        # TODO: Make not Blocking
        return self.uartPort.read(limit)

    def senddata(self, messagelist):
        # Prepare Message List for Sending
        message = bytearray(messagelist)

        # Flush Buffers
        # self.uartPort.flushInput()
        # self.uartPort.flushOutput()

        # Send
        print "Sending: ", messagelist
        self.uartPort.write(message)

        return True  # TODO: Verify Transmission

