#!/usr/bin/env python

# Copyright (c) 2015, Andoni Aguirrezabal
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Thread Components
import threading

# Socket Components
import socket

# Feaux Server Components
import random


class MainApplication:
    def __init__(self):
        # Variable Init
        self.adcOneData = [0, 0]
        self.adcTwoData = [0, 0, 0, 0, 0, 0, 0, 0]

        # TCP Socket Init
        self.SensorSocketTarget = ("0.0.0.0", 2325)
        self.SensorSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.SensorSocket.bind(self.SensorSocketTarget)

        # Clients List
        self.SensorSocketClientList = []

        # ADC Reader Module Init
        self.adcReaderThread = threading.Thread(target=self.adcLoop)
        self.adcReaderThread.start()

    def __del__(self):
        self.SensorSocket.close()

    def readAdc(self):
        for i in range(0, len(self.adcOneData)):
            self.adcOneData[i] = random.randint(0, 4096)

        for i in range(0, len(self.adcTwoData)):
            self.adcTwoData[i] = random.randint(0, 1024)

    def adcLoop(self):
        while True:
            # Get The Data
            self.readAdc()
            outputMessage = ""

            # Build Message
            for i in range(0, len(self.adcOneData)):
                outputMessage = outputMessage + str(self.adcOneData[i]).zfill(4) + ":"

            for i in range(0, len(self.adcTwoData)):
                outputMessage = outputMessage + str(self.adcTwoData[i]).zfill(4) + ":"

            outputMessage = outputMessage + "\n"

            for client in self.SensorSocketClientList:
                try:
                    client.send(outputMessage)
                except socket.error:
                    client.close()  # Assume User Disconnected
                    self.SensorSocketClientList.remove(client)

    def main(self):
        self.SensorSocket.listen(1)
        while True:
            clientConnection, clientAddr = self.SensorSocket.accept()
            self.SensorSocketClientList.append(clientConnection)
            print "New Sensor Client Connected: ", clientAddr

if __name__ == "__main__":
    app = MainApplication()
    app.main()
