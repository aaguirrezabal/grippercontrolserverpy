# Copyright (c) 2014, Andoni Aguirrezabal
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# SPI Interface
import spidev

# ADC Types
MCP3008 = 0
MCP3202 = 1


class ADCReader:
    def __init__(self, spiBus = 0, spiDevice = 0, adcType = 0):
        # SPI Interface Setup
        self.spiInterface = spidev.SpiDev()
        self.spiBus = spiBus
        self.spiDevice = spiDevice

        if (adcType > 1) or (adcType < 0):
            self.adcType = 0
        else:
            self.adcType = adcType

        # ADC Config
        if (self.adcType == 0):
            # MCP3008
            self.adcChannels = 8
            self.adcShiftAmt = 4
            self.adcBitMask = 3
        elif (self.adcType == 1):
            # MCP3202
            self.adcChannels = 2
            self.adcShiftAmt = 6
            self.adcBitMask = 15

        self.adcMax = self.adcChannels - 1
        self.open()

    def __del__(self):
        self.spiInterface.close()

    def open(self):
        self.spiInterface.open(self.spiBus, self.spiDevice)

    def readadc(self, channelnumber):
        if (channelnumber > self.adcMax) or (channelnumber < 0):
            return -1
        request = self.spiInterface.xfer2([1, ((self.adcChannels + channelnumber) << self.adcShiftAmt), 0])
        return (((request[1] & self.adcBitMask) << 8) + request[2])
